package com.skosc.tkfhw

import androidx.annotation.ColorRes

data class Station(val name: String, @ColorRes val color: Int) {
    companion object {
        @JvmStatic fun makeRedLine(name: String) = Station(name, R.color.station_red)
        @JvmStatic fun makeBlueLine(name: String) = Station(name, R.color.station_blue)
    }
}