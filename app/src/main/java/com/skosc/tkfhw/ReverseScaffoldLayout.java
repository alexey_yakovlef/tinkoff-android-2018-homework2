package com.skosc.tkfhw;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class ReverseScaffoldLayout extends ViewGroup {
    private static final int MAX_ROW_COUNT = 32;

    final int deviceWidth; // Total View width
    private final int[] rowOffset = new int[MAX_ROW_COUNT];

    public ReverseScaffoldLayout(Context context) {
        this(context, null, 0);
    }

    public ReverseScaffoldLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReverseScaffoldLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        final Point deviceDisplay = new Point();
        display.getSize(deviceDisplay);
        deviceWidth = deviceDisplay.x;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        // Current child's dimensions
        int curWidth = 0;
        int curHeight = 0;

        // Child
        final int childLeft = this.getPaddingLeft();
        final int childTop = this.getPaddingTop();
        final int childRight = this.getMeasuredWidth() - this.getPaddingRight();
        final int childBottom = this.getMeasuredHeight() - this.getPaddingBottom();
        final int childWidth = childRight - childLeft;
        final int childHeight = childBottom - childTop;

        int row = -1; // Take in account first auto increase
        int maxHeight = 0;

        int curLeft = childLeft;
        int curTop = childTop;

        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child.getVisibility() == GONE)
                return;

            // Measure children with given specs
            int widthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.AT_MOST);
            int heightMeasureSpec = MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.AT_MOST);
            child.measure(widthMeasureSpec, heightMeasureSpec);

            curWidth = child.getMeasuredWidth();
            curHeight = child.getMeasuredHeight();
            if (curLeft + curWidth >= childRight) {
                curLeft = childLeft;
                curTop += maxHeight;
                maxHeight = 0;
            }

            if (curLeft == 0) {
                row++;
            }

            child.layout(
                    rowOffset[row] + curLeft, // Take in account calculated offset from left
                    curTop,
                    rowOffset[row] + curLeft + curWidth,
                    curTop + curHeight
            );

            maxHeight = Math.max(maxHeight, curHeight);
            curLeft += curWidth;
        }
    }

    /*
        Algorithm description:
        Take views one by one and calculate it's width.
        Sum view's widths until they can't fit in a row,
        then calculate left space and put it in rowOffset for current row for later use in onLayout,
        then increase row and start all over again for all views.
        After all views was precessed, add last row offset to rowOffset.
        Call setMeasuredDimension with calculated total height
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Clear array
        for (int i = 0; i < rowOffset.length; i++) {
            rowOffset[i] = 0;
        }

        int row = 0;
        int curRowWidth = 0;
        int curRowHeight = 0;
        int totalHeight = 0;
        int childState = 0;

        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() == GONE)
                return;
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            childState = combineMeasuredStates(childState, child.getMeasuredState());
            if (curRowWidth + child.getMeasuredWidth() < deviceWidth) {
                curRowWidth += child.getMeasuredWidth();
                curRowHeight = Math.max(curRowHeight, child.getMeasuredHeight());
            } else {
                rowOffset[row] = deviceWidth - curRowWidth;
                row++;
                curRowWidth = child.getMeasuredWidth();
                totalHeight += curRowHeight;
                curRowHeight = 0;
            }
        }

        // Add last row
        rowOffset[row] = deviceWidth - curRowWidth;

        setMeasuredDimension(resolveSizeAndState(deviceWidth, widthMeasureSpec, childState),
                resolveSizeAndState(totalHeight, heightMeasureSpec, childState << MEASURED_HEIGHT_STATE_SHIFT));
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        Animation animation = AnimationUtils.loadAnimation(child.getContext(), R.anim.slidex);
        child.startAnimation(animation);
    }
}
